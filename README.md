# Aero Record Control

Android audio recording app where the server app on one cellphone can control the recording of several client apps on other cellphones.

The software consists of two applications: for client(s) and server.

Client can be used as a standalone audio recorder, but can be controlled via server application as well. Server application is intended to provide ability to control (= start/stop/pause recording) multiple clients via wireless (Wi-Fi) connection.

## Gratitudes

The initial idea, feedback and funding have been provided by [muonIT](https://muon.de), Udo Puetz.

## How to use

- Open applications, enable wi-fi and connect to access point;
- Setup client (change server address according to information in server application);
- Start server;
- Connect client to server (Side menu > Server > Connect).

## License information

```
    Copyright (C) 2022 Anri Dellal

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
