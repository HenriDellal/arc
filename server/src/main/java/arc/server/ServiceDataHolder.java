package arc.server;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

public class ServiceDataHolder {
	private MutableLiveData<List<TCPClient>> clientsData;
	private MutableLiveData<Boolean> isServiceRunning;

	public void setClients(List<TCPClient> clients) {
		clientsData.postValue(clients);
	}

	public LiveData<List<TCPClient>> getClients() {
		return clientsData;
	}

	public void setServiceState(boolean running) {
		isServiceRunning.postValue(running);
	}

	public LiveData<Boolean> getServiceState() {
		return isServiceRunning;
	}

	public static class InstanceHolder {
		public static final ServiceDataHolder INSTANCE = new ServiceDataHolder();
	}

	public static ServiceDataHolder getInstance() {
		return InstanceHolder.INSTANCE;
	}

	public ServiceDataHolder() {
		ArrayList<TCPClient> clients = new ArrayList<TCPClient>();
		clientsData = new MutableLiveData<List<TCPClient>>(clients);
		isServiceRunning = new MutableLiveData<>(false);
	}
}
