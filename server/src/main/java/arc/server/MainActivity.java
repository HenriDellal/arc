package arc.server;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.text.format.Formatter;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

	private static final String[] PERMISSIONS = new String[]{
			Manifest.permission.ACCESS_WIFI_STATE,
			Manifest.permission.CHANGE_WIFI_STATE,
			Manifest.permission.ACCESS_FINE_LOCATION
	};
	private EditText portField;

	private Intent serverServiceIntent;

	private IntentFilter intentFilter;
	private WifiManager manager;
	private WifiBroadcastReceiver wifiBroadcastReceiver;
	private int port;

	private RecyclerView.LayoutManager layoutManager;

	private LiveData<List<TCPClient>> clientsLiveData;
	private LiveData<Boolean> serviceStateLiveData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (Build.VERSION.SDK_INT >= 23) {
			for (String permission : PERMISSIONS) {
				if (checkSelfPermission(permission) == PackageManager.PERMISSION_DENIED) {
					requestPermissions(PERMISSIONS, 0);
					break;
				}
			}
		}
		port = 8080;
		portField = (EditText) findViewById(R.id.edit_text_port);
		portField.setText(Integer.toString(port));

		clientsLiveData = ServiceDataHolder.getInstance().getClients();
		clientsLiveData.observe(this, new Observer<List<TCPClient>>() {
			@Override
			public void onChanged(List<TCPClient> tcpClients) {
				adapter.setClients(tcpClients);
			}
		});

		serviceStateLiveData = ServiceDataHolder.getInstance().getServiceState();
		serviceStateLiveData.observe(this, new Observer<Boolean>() {
			@Override
			public void onChanged(Boolean isServiceRunning) {
				if (isServiceRunning) {
					findViewById(R.id.port_panel).setVisibility(View.GONE);
					findViewById(R.id.btn_stop_server).setVisibility(View.VISIBLE);
				} else {
					findViewById(R.id.port_panel).setVisibility(View.VISIBLE);
					findViewById(R.id.btn_stop_server).setVisibility(View.INVISIBLE);
				}
			}
		});

		findViewById(R.id.btn_start_server).setOnClickListener(this);

		RecyclerView clientListView = (RecyclerView) findViewById(R.id.client_list);
		layoutManager = new LinearLayoutManager(this);
		clientListView.setLayoutManager(layoutManager);
		adapter = new ClientAdapter(this);

		clientListView.setAdapter(adapter);

		serverServiceIntent = new Intent(this, TCPServerService.class);
		manager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
		intentFilter = new IntentFilter();
		intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
		intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
		wifiBroadcastReceiver = new WifiBroadcastReceiver(this);
		findViewById(R.id.btn_copy_address).setOnClickListener(this);
		findViewById(R.id.btn_stop_server).setOnClickListener(this);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		for (int i : grantResults) {
			if (i == PackageManager.PERMISSION_DENIED) {
				finish();
			}
		}
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(wifiBroadcastReceiver, intentFilter);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(wifiBroadcastReceiver);
	}

	@Override
	protected void onDestroy() {
		serviceStateLiveData.removeObservers(this);
		clientsLiveData.removeObservers(this);
		super.onDestroy();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btn_start_server:
				try {
					port = Integer.parseInt(portField.getText().toString());
				} catch (NumberFormatException e) {
					e.printStackTrace();
					Toast.makeText(this, R.string.port_is_not_correct, Toast.LENGTH_LONG).show();
					return;
				}

				if (port < 1024 || port > 65535) {
					Toast.makeText(this, R.string.ports_error, Toast.LENGTH_LONG).show();
					return;
				}
				Intent startIntent = new Intent(this, TCPServerService.class);
				startIntent.setAction(TCPServerService.ACTION_START);
				startIntent.putExtra(TCPServerService.PORT_EXTRA, port);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
					startForegroundService(startIntent);
				} else {
					startService(startIntent);
				}
				break;
			case R.id.btn_copy_address:
				if (null != ipString) {
					ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
					ClipData clip = ClipData.newPlainText("none", ipString);
					clipboard.setPrimaryClip(clip);
					Toast.makeText(this, R.string.msg_copied_to_clipboard, Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.btn_stop_server:
				Intent stopIntent = new Intent(this, TCPServerService.class);
				stopIntent.setAction(TCPServerService.ACTION_STOP);
				startService(stopIntent);
		}
	}

	private String ipString;

	public void onWifiStateChanged(int wifiState) {
		switch (wifiState) {
			case WifiManager.WIFI_STATE_ENABLED:
				int ip = manager.getConnectionInfo().getIpAddress();
				String serverPanelInfo;
				if (ip == 0) {
					serverPanelInfo = getResources().getString(R.string.msg_connect_to_wifi_ap);
					ipString = null;
				} else {
					ipString = Formatter.formatIpAddress(ip);
					serverPanelInfo = ipString;
				}
				findViewById(R.id.server_ip_panel).setVisibility(View.VISIBLE);
				((TextView)findViewById(R.id.server_ip_text)).setText(serverPanelInfo);

				break;
			case WifiManager.WIFI_STATE_DISABLED:
				((TextView)findViewById(R.id.server_ip_text)).setText(R.string.msg_disabled_wifi);
			default:
				ipString = null;
		}
	}

	private ClientAdapter adapter;
}