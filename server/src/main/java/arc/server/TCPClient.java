package arc.server;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.LinkedList;

import arc.message.ClientEntity;
import arc.message.InitMessage;
import arc.message.Message;
import arc.message.MessageFactory;
import arc.message.NameChangeMessage;
import arc.message.NotifyMessage;
import arc.message.StatsMessage;

public class TCPClient {

	private boolean running;

	private Socket socket;
	private String name;

	private TCPServerService service;
	private TCPInputWorker inputWorker;
	private TCPOutputWorker outputWorker;
	private ClientEntity clientEntity;

	public TCPClient(TCPServerService service, Socket socket) {
		this.service = service;
		this.socket = socket;
		running = true;
		inputWorker = new TCPInputWorker();
		outputWorker = new TCPOutputWorker();

		new Thread(inputWorker).start();
		new Thread(outputWorker).start();
	}

	public ClientEntity getClientEntity() {
		return clientEntity;
	}

	public TCPOutputWorker getOutputWorker() {
		return outputWorker;
	}

	public boolean isRunning() {
		return running;
	}

	public void stop() {
		running = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public class TCPInputWorker implements Runnable {
		private LinkedList<Message> messageQueue;
		public TCPInputWorker() {
			Log.e("arc.server", "TCPInputWorker was created");
			messageQueue = new LinkedList<>();
		}

		private void processMessage(BufferedReader br) throws IOException {
			final Message message = MessageFactory.create(br);
			if (message == null) {
				throw new IOException();
			}
			if (message instanceof InitMessage) {
				name = message.getData();
				clientEntity = new ClientEntity(message.getData());
				Log.e("arc.server", "New client added");

			} else if (message instanceof NameChangeMessage) {
				NameChangeMessage ncm = (NameChangeMessage) message;
				clientEntity.setState(ncm);
				clientEntity.setName(ncm.getNewName());
			} else if (message instanceof NotifyMessage) {
				NotifyMessage notifyMessage = (NotifyMessage) message;
				Log.e("arc.server", "notification received");

				clientEntity.setState(notifyMessage);

			} else if (message instanceof StatsMessage) {
				StatsMessage stats = (StatsMessage) message;
				clientEntity.setStats(stats);
			}
			service.updateServiceDataHolder();
		}

		@Override
		public void run() {
			Log.i("arc.server", "Input run started");
			InputStream inputStream = null;
			BufferedReader bufferedReader = null;
			try {
				inputStream = socket.getInputStream();
				bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			} catch (IOException e) {
				e.printStackTrace();
				if (null != socket) {
					try {
						socket.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
				return;
			}

			short errorCounter = 0;
			while (running) {
				Log.i("arc.server", "reading messages in server loop");
				try {
					processMessage(bufferedReader);
				} catch (IOException e) {
					e.printStackTrace();
					errorCounter++;
					if (errorCounter >= 3) {
						running = false;
					}
				}
			}
			if (null != socket && !socket.isClosed()) {
				Log.e("arc.server", "Close socket");
				try {
					socket.close();
					//serverSocket.close();
					socket = null;
					//serverSocket = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				if (null != bufferedReader) {
					Log.e("arc.server", "Close IO streams");
					bufferedReader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			service.disconnect(TCPClient.this);
		}
	}

	public class TCPOutputWorker implements Runnable {
		private LinkedList<Message> messageQueue;
		private int port;
		public TCPOutputWorker() {
			Log.e("arc.server", "TCPOutputWorker was created");
			messageQueue = new LinkedList<>();
		}

		public void addMessage(Message message) {
			messageQueue.add(message);
		}

		@Override
		public void run() {
			Log.i("arc.server", "Output run started");
			DataOutputStream outputStream = null;
			try {
				outputStream = new DataOutputStream(socket.getOutputStream());
			} catch (IOException e) {
				e.printStackTrace();
				if (null != socket) {
					try {
						socket.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
				return;
			}

			short errorCounter = 0;
			while (running) {
				try {
					while (!messageQueue.isEmpty()) {
						Log.i("arc.server", "sending messages in server loop");
						MessageFactory.send(outputStream, messageQueue.removeFirst());
					}
				} catch (IOException e) {
					e.printStackTrace();
					errorCounter++;
					if (errorCounter >= 3) {
						running = false;
					}
				}
			}
			if (null != socket && !socket.isClosed()) {
				Log.e("arc.server", "Close socket");
				try {
					socket.close();
					//serverSocket.close();
					socket = null;
					//serverSocket = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				if (null != outputStream) {
					Log.e("arc.server", "Close IO streams");
					outputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			service.disconnect(TCPClient.this);
		}
	}
}
