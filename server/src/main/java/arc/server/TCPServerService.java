package arc.server;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.LifecycleService;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class TCPServerService extends LifecycleService {

	public static final String ACTION_STOP = "arc.server.stop";
	public static final String ACTION_START = "arc.server.start";
	public static final String PORT_EXTRA = "port_extra";

	private boolean running;
	private int port;

	private ServerBinder binder = new ServerBinder();

	private ArrayList<TCPClient> tcpClients;

	private ServerSocket serverSocket;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		String action = intent.getAction();
		if (ACTION_STOP.equals(action)) {
			stopForeground(true);
			stopSelf();
		} else if (ACTION_START.equals(action)) {
			port = intent.getIntExtra(PORT_EXTRA, 8080);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				createForegroundNotification();
			}
			start();
		}
		return START_STICKY;
	}
	public static final String SERVICE_ID = "arc.server.service_notification";

	@RequiresApi(api = Build.VERSION_CODES.O)
	private void createForegroundNotification() {
		NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		nm.createNotificationChannelGroup(new NotificationChannelGroup(SERVICE_ID, "ARC Server"));

		NotificationChannel channel = new NotificationChannel(SERVICE_ID, "Server notifications",
				NotificationManager.IMPORTANCE_MIN);
		nm.createNotificationChannel(channel);
		channel.enableLights(false);
		channel.setLockscreenVisibility(Notification.VISIBILITY_SECRET);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this, SERVICE_ID);
		builder.setContentTitle("Server service is running")
				.setTicker("Server is running")
				.setContentText("")
				.setWhen(0)
				.setOnlyAlertOnce(true)
				.setOngoing(true)
				.setChannelId(SERVICE_ID);
		Notification notification = builder.build();
		startForeground(1, notification);
	}

	private void start() {
		updateServiceState(true);
		try {
			if (null != serverSocket) {
				serverSocket.close();
				serverSocket = null;
			}
			serverSocket = new ServerSocket(port);
			serverSocket.setReuseAddress(true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (running) {
					try {
						Socket socket = serverSocket.accept();
						TCPClient tcpClient = new TCPClient(TCPServerService.this, socket);
						tcpClients.add(tcpClient);
						updateServiceDataHolder();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

	}

	public class ServerBinder extends Binder {
		TCPServerService getService() {
			return TCPServerService.this;
		}
	}

	private void stop() {
		updateServiceState(false);
		for (TCPClient client: tcpClients) {
			client.stop();
		}
		try {
			if (null != serverSocket) {
				serverSocket.close();
				serverSocket = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		tcpClients.clear();
		updateServiceDataHolder();
	}

	public synchronized void disconnect(TCPClient client) {
		client.stop();
		tcpClients.remove(client);
		updateServiceDataHolder();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		tcpClients = new ArrayList<>();
		updateServiceDataHolder();
	}

	@Override
	public void onDestroy() {
		stop();
		super.onDestroy();
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		super.onBind(intent);
		return binder;
	}

	public void updateServiceDataHolder() {
		ServiceDataHolder.getInstance().setClients(tcpClients);
	}
	private void updateServiceState(boolean isRunning) {
		running = isRunning;
		ServiceDataHolder.getInstance().setServiceState(running);
	}
}
