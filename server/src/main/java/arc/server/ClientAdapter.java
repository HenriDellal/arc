package arc.server;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import arc.message.ClientEntity;
import arc.message.CommandMessage;
import arc.message.Prefix;

public class ClientAdapter extends RecyclerView.Adapter<ClientAdapter.ViewHolder> {

	private MainActivity activity;
	private List<TCPClient> clients;

	public ClientAdapter(MainActivity activity) {
		super();
		this.activity = activity;
	}

	public void setClients(List<TCPClient> clients) {
		this.clients = clients;
		notifyDataSetChanged();
	}

	private void showStats(ClientEntity client) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle(client.getName());
		builder.setCancelable(true);
		final TextView textView = new TextView(activity);
		textView.setText(client.getStatsData());
		builder.setView(textView);

		builder.create().show();
	}

	protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		TextView name;
		TextView time;
		Button btnDisconnect;
		Button btnStop;
		Button btnRecord;
		Button btnStats;

		int state;

		public ViewHolder(View view) {
			super(view);
			name = (TextView) view.findViewById(R.id.text_client_name);
			time = (TextView) view.findViewById(R.id.text_recording_time);
			btnDisconnect = (Button) view.findViewById(R.id.btn_client_disconnect);
			btnStop = (Button) view.findViewById(R.id.btn_client_stop);
			btnRecord = (Button) view.findViewById(R.id.btn_client_record);
			btnStats = (Button) view.findViewById(R.id.btn_client_stats);
			state = Prefix.UNKNOWN_STATE;

			view.setTag(this);
			btnDisconnect.setOnClickListener(this);
			btnRecord.setOnClickListener(this);
			btnStats.setOnClickListener(this);
			btnStop.setOnClickListener(this);

		}

		@Override
		public void onClick(View view) {
			int position = getLayoutPosition();
			TCPClient tcpClient = clients.get(position);
			ClientEntity client = tcpClient.getClientEntity();
			CommandMessage msg;
			switch (view.getId()) {
				case R.id.btn_client_stats:
					showStats(client);
					break;
				case R.id.btn_client_record:
					msg = new CommandMessage();
					msg.setMagic(Prefix.SERVER_MAGIC);
					msg.setName(client.getName());
					String cmd;
					if (state == Prefix.STATE_START_RECORDING) {
						cmd = Prefix.CMD_PAUSE_RECORDING;
					} else {
						cmd = Prefix.CMD_START_RECORDING;
					}
					msg.setCmd(cmd);
					tcpClient.getOutputWorker().addMessage(msg);
					break;
				case R.id.btn_client_stop:
					msg = new CommandMessage();
					msg.setMagic(Prefix.SERVER_MAGIC);
					msg.setName(client.getName());
					msg.setCmd(Prefix.CMD_STOP_RECORDING);
					tcpClient.getOutputWorker().addMessage(msg);
					break;
				case R.id.btn_client_disconnect:
					tcpClient.stop();
					notifyDataSetChanged();
					break;
				default:
					return;
			}
		}
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.client_card, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		ClientEntity client = clients.get(position).getClientEntity();
		if (null == client)
			return;

		holder.name.setText(client.getName());
		holder.time.setText(client.getFormattedTime());
		int clientState = client.getState();
		switch (clientState) {
			case Prefix.STATE_START_RECORDING:
				holder.btnStop.setVisibility(View.VISIBLE);
				holder.btnRecord.setText("pause");
				holder.state = clientState;
				break;
			case Prefix.STATE_PAUSE_RECORDING:
				holder.btnStop.setVisibility(View.VISIBLE);
				holder.btnRecord.setText("resume");
				holder.state = clientState;
				break;
			case Prefix.STATE_STOP_RECORDING:
				holder.btnStop.setVisibility(View.GONE);
				holder.btnRecord.setText("start");
				holder.state = clientState;
				break;
			case Prefix.NOTIFY_NAME_CHANGE_STATE:
				holder.name.setText(client.getName());
				break;
			case Prefix.UNKNOWN_STATE:
				break;
		}

		if (null != client.getStatsData()) {
			holder.btnStats.setVisibility(View.VISIBLE);
		} else {
			holder.btnStats.setVisibility(View.GONE);
		}
	}

	@Override
	public int getItemCount() {
		return (null != clients) ? clients.size() : 0;
	}
}
