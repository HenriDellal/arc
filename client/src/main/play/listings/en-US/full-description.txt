ARC Client is a recorder application that can be controlled wirelessly by <a href="https://f-droid.org/en/packages/arc.server/">ARC Server</a>.
The application can be used in offline mode as standalone recorder.

Wi-Fi connection is needed to use the application with server.

<b>Credits:</b>

The initial idea, feedback and funding have been provided by <a href="https://muon.de">muonIT</a>, Udo Puetz.