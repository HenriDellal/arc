package arc.client;

import android.content.Context;
import android.util.AttributeSet;

import androidx.preference.ListPreference;

import java.util.HashMap;
import java.util.Map;

public class AudioFormatPreference extends ListPreference {

	public static final Map<String, String> ENTRIES;
	static {
		ENTRIES = new HashMap<>();
		ENTRIES.put(EncoderHelper.OUTPUT_TYPE_3GPP, "3GPP");
		ENTRIES.put(EncoderHelper.OUTPUT_TYPE_AMR_WB, "AMR_WB");
		ENTRIES.put(EncoderHelper.OUTPUT_TYPE_MP4, "MP4");
	}

	public AudioFormatPreference(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		Encoder[] encoders = EncoderHelper.getEncoders();
		CharSequence[] entryValues = new CharSequence[encoders.length+1];
		CharSequence[] entries = new CharSequence[encoders.length+1];
		entryValues[0] = EncoderHelper.OUTPUT_TYPE_CUSTOM_WAV;
		entries[0] = "WAV";
		for (int i = 1; i < encoders.length+1; i++) {
			entryValues[i] = encoders[i-1].getType();
			entries[i] = ENTRIES.get(encoders[i-1].getType());
		}
		setEntryValues(entryValues);
		setEntries(entries);
	}
}
