package arc.client;

import android.content.Context;
import android.media.MediaRecorder;
import android.os.Environment;

import androidx.preference.PreferenceManager;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class Utils {
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.UK);

	public static int toLittleEndian(int a) {
		int result = 0;
		for (int i = 24; i >= 0; i -= 8) {
			result += ( ((a & (0xff << i)) >>> i) << (24 - i) );
		}
		return result;
	}

	public static String getFileExtension(int outputFormat) {
		switch (outputFormat) {
			case MediaRecorder.OutputFormat.THREE_GPP:
				return "3gpp";
			case MediaRecorder.OutputFormat.AMR_WB:
				return "amr";
			case MediaRecorder.OutputFormat.MPEG_4:
				return "mp4";
			default:
				return "aac";
		}
	}

	public static File getRecordingFile(Context context, String fileExtension) {
		File file;
		File musicDir = context.getExternalFilesDir(Environment.DIRECTORY_MUSIC);
		if (!musicDir.exists()) {
			musicDir.mkdirs();
		}
		String clientName = PreferenceManager.getDefaultSharedPreferences(context).getString("client_name", null);
		String filename = String.format("%s-%s.%s", clientName, dateFormat.format(new Date()), fileExtension);
		file = new File(musicDir, filename);

		if (file.exists()) {
			int i = 1;
			String[] s = filename.split(".");
			do {
				file = new File(musicDir, filename);
				filename = String.format("%s-%d.%s", s[0], i, s[1]);
				i++;
			} while (file.exists());
		}

		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}

	public static String generateClientName() {
		return UUID.randomUUID()
				.toString()
				.replaceAll("-", "")
				.substring(0, 8);
	}
}
