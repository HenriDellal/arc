package arc.client;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.preference.PreferenceManager;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.LinkedList;

import arc.message.CommandMessage;
import arc.message.InitMessage;
import arc.message.Message;
import arc.message.MessageFactory;
import arc.message.NameChangeMessage;
import arc.message.NotifyMessage;
import arc.message.Prefix;
import arc.message.StatsMessage;

public class TCPClientService extends Service {

	private final IBinder binder = new ClientBinder();

	private MainActivity activity;
	private String name;
	private String address;
	private int port;

	public String getAddress() {
		return String.format("%s:%d", address, port);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (null != outputWorker) {
			NameChangeMessage ncm = MessageFactory.formNameChangeMessage(
					Prefix.CLIENT_MAGIC,
					this.name,
					name
			);
			outputWorker.sendMessage(ncm);
		}
		this.name = name;
	}

	public TCPClientService() {
		prevState = -1;
	}

	public class ClientBinder extends Binder {
		TCPClientService getService() {
			return TCPClientService.this;
		}
	}

	private int prevState;
	private synchronized void notifyClientStateChange(final int state) {
		if (prevState == state) {
			return;
		}
		prevState = state;
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				activity.notifyClientStateChange(state);
			}
		});
	}

	private boolean running;

	private TCPInputWorker inputWorker;

	public TCPOutputWorker getOutputWorker() {
		return outputWorker;
	}

	private TCPOutputWorker outputWorker;

	public void start(MainActivity activity, String address, int port) {
		this.activity = activity;
		running = true;
		inputWorker = null;
		outputWorker = null;
		this.address = address;
		this.port = port;
		try {
			InetAddress inetAddress = InetAddress.getByName(address);

			outputWorker = new TCPOutputWorker(inetAddress, port);
			new Thread(outputWorker).start();
			inputWorker = new TCPInputWorker(outputWorker, inetAddress, port);
			new Thread(inputWorker).start();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isRunning() {
		return running;
	}

	public void stop() {
		running = false;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	private Socket socket;

	private synchronized void initSocket(InetAddress inetAddress, int port) throws IOException {
		if (null != socket)
			return;

		socket = new Socket(inetAddress, port);
		socket.setReuseAddress(true);
	}

	public class TCPInputWorker implements Runnable {
		private InetAddress inetAddress;
		private int port;
		private short errorCounter;
		private TCPOutputWorker outputWorker;
		public TCPInputWorker(TCPOutputWorker outputWorker, InetAddress inetAddress, int port) {
			this.outputWorker = outputWorker;
			this.inetAddress = inetAddress;
			this.port = port;
		}

		private void processMessage(BufferedReader br) throws IOException {
			final Message message = MessageFactory.create(br);

			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (message instanceof InitMessage) {
						Log.i("arc.client", "Received init message from server");
						NotifyMessage nm = activity.getService().getNotificationMessage(name);
						outputWorker.sendMessage(nm);
					} else if (message instanceof CommandMessage) {
						CommandMessage cmdMessage = (CommandMessage) message;
						String command = cmdMessage.getCmd();
						Recorder recorder = activity.getService().getRecorder();
						Log.e("arc.client", command);
						switch (command) {
							case Prefix.CMD_START_RECORDING:
								if (!recorder.isRecording()) {
									System.out.println("start recording via wifi");
									activity.getService().toggleRecording(activity);
									activity.updateRecordingControls();
								} else if (recorder.isPaused()) {
									activity.getService().togglePause(activity);
									activity.updateRecordingControls();
								} else {
									Log.e("arc.client", "Error: Recorder is active");
								}
								break;
							case Prefix.CMD_PAUSE_RECORDING:
								if (recorder.isRecording()) {
									activity.getService().togglePause(activity);
									activity.updateRecordingControls();
								} else {
									Log.e("arc.client", "Error: Recorder is not active");
								}
								break;
							case Prefix.CMD_STOP_RECORDING:
								if (activity.getService().getRecorder().isRecording()) {
									activity.getService().toggleRecording(activity);
									activity.updateRecordingControls();
								} else {
									Log.e("arc.client", "Error: Recorder is not active");

								}
								break;
							case Prefix.CMD_GET_RECORDING_STATUS:
								float duration = activity.getService().getRecorder().getDuration();
								StatsMessage sm = MessageFactory.formStatsMessage(
										Prefix.CLIENT_MAGIC,
										name,
										Prefix.INFO_RECORD_STATS
								);
								outputWorker.sendMessage(sm);
								break;
							case Prefix.CMD_SET_NAME:
								SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
								preferences.edit().putString("client_name", cmdMessage.getData()).apply();
								break;
							default:
								Log.e("arc.client", String.format("Unknown command %s", command));
						}
					} else {
						errorCounter++;
					}
				}
			});
		}

		@Override
		public void run() {
			InputStream inputStream = null;
			BufferedReader bufferedReader = null;
			try {
				initSocket(inetAddress, port);
				inputStream = socket.getInputStream();
				bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			} catch (IOException e) {
				e.printStackTrace();
				running = false;
			}

			if (running) {
				notifyClientStateChange(ClientState.CONNECTION_STARTED);
			}

			while (running) {
				try {
					processMessage(bufferedReader);
				} catch (IOException e) {
					e.printStackTrace();
					errorCounter++;
				}
				if (errorCounter >= 3) {
					running = false;
				}
			}

			try {
				Log.e("arc.client", "closing connection");
				if (null != bufferedReader) {
					bufferedReader.close();
				}

				if (null != socket) {
					socket.close();
					socket = null;
				}
				notifyClientStateChange(ClientState.CONNECTION_CLOSED);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private DataOutputStream outputStream;

	public class TCPOutputWorker implements Runnable {

		private InetAddress inetAddress;
		private int port;
		private LinkedList<Message> messageQueue;
		public TCPOutputWorker(InetAddress inetAddress, int port) {
			this.inetAddress = inetAddress;
			this.port = port;
			messageQueue = new LinkedList<>();
			InitMessage message = MessageFactory.formInitMessage(Prefix.CLIENT_MAGIC, name);
			sendMessage(message);
		}

		public void sendMessage(Message message) {
			if (message != null)
				messageQueue.add(message);
		}

		@Override
		public void run() {
			outputStream = null;
			try {
				initSocket(inetAddress, port);
				outputStream = new DataOutputStream(socket.getOutputStream());
			} catch (IOException e) {
				e.printStackTrace();
				running = false;
			}

			if (running) {
				notifyClientStateChange(ClientState.CONNECTION_STARTED);
			}

			short errorCounter = 0;
			while (running) {
				try {
					while (!messageQueue.isEmpty()) {
						Log.e("arc.client", "sending a message");
						MessageFactory.send(outputStream, messageQueue.removeFirst());

						Log.e("arc.client", "message was sent");
					}

				} catch (IOException e) {
					e.printStackTrace();
					errorCounter++;
					if (errorCounter >= 3) {
						running = false;
					}
				}
			}

			try {
				Log.e("arc.client", "closing connection");
				if (null != outputStream) {
					outputStream.flush();
					outputStream.close();
					outputStream = null;
				}

				if (null != socket) {
					socket.close();
					socket = null;
				}
				notifyClientStateChange(ClientState.CONNECTION_CLOSED);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
