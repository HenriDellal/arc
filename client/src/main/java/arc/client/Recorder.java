package arc.client;

import android.content.Context;

public interface Recorder {
	void start(Context context);
	void stop();
	boolean isRecording();
	void pause();
	void resume();
	float getDuration();
	String getFilename();
	boolean isPaused();
}
