package arc.client;

import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.ListPreference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
		setPreferencesFromResource(R.xml.root_preferences, rootKey);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		updateAudioPreferences();
	}

	private void updateAudioPreferences() {
		PreferenceScreen prefScreen = this.getPreferenceScreen();
		PreferenceCategory audioCategory = (PreferenceCategory) prefScreen.findPreference("pc_audio");
		AudioFormatPreference afp = (AudioFormatPreference) audioCategory.findPreference("audio_format");
		PreferenceCategory WAVCategory = (PreferenceCategory) findPreference("pc_wav");
		WAVCategory.setVisible("WAV".contentEquals(afp.getEntry()));
		if (WAVCategory.isVisible()) {
			updateSampleRatePreference();
		}
	}

	private void updateSampleRatePreference() {
		PreferenceCategory WAVCategory = (PreferenceCategory) findPreference("pc_wav");
		SharedPreferences prefs = getPreferenceManager().getSharedPreferences();
		int inputType = Integer.parseInt(
				prefs.getString("input_channel_type",
						String.valueOf(AudioFormat.CHANNEL_IN_MONO))
		);
		int encoderType = Integer.parseInt(
				prefs.getString("encoding_type",
						String.valueOf(AudioFormat.CHANNEL_IN_MONO))
		);
		ListPreference sampleRatePref = WAVCategory.findPreference("sample_rate_wav");

		CharSequence[] sampleRates = SampleRateHelper.getSampleRates(inputType, encoderType);
		sampleRatePref.setEntries(sampleRates);
		sampleRatePref.setEntryValues(sampleRates);
	}

	@Override
	public void onResume() {
		super.onResume();
		getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals("client_name")) {
			TCPClientService service = ((MainActivity) getActivity()).getTCPService();
			if (null != service)
				service.setName(sharedPreferences.getString(key, null));
		} else if (!key.startsWith("server_")) {
			RecordingService service = ((MainActivity) getActivity()).getService();
			if (null != service && !service.hasPlannedRecorderUpdate()) {
				Toast.makeText(getContext(), getResources().getString(R.string.msg_preference_changes), Toast.LENGTH_SHORT).show();
				service.updateRecorderPrefs();
			}
		}
		if (key.startsWith("audio_")) {
			updateAudioPreferences();
		} else if (key.equals("input_channel_type") || key.equals("encoding_type")) {
			updateSampleRatePreference();
		}
	}
}