package arc.client;

import android.media.AudioFormat;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class WaveConfig {
	private int sampleRate;
	private int inputType;
	private int encodingType;
	private byte bytesPerSample;

	// see http://www-mmsp.ece.mcgill.ca/Documents/AudioFormats/WAVE/WAVE.html
	// for WAVE format specification

	private static byte[] HEADER_TEMPLATE_PCM = new byte[] {
			0x52, 0x49, 0x46, 0x46, // "RIFF"
			   0,    0,    0,    0, // Size of the file (32-bit integer)
			0x57, 0x41, 0x56, 0x45, // "WAVE"
			0x66, 0x6D, 0x74, 0x20, // "fmt "
			0x10,    0,    0,    0, // length of format data
			             0x1,    0, // format type (1 - PCM)
			               0,    0, // number of channels
			   0,    0,    0,    0, // Sampling rate (blocks per second)
			   0,    0,    0,    0, // Data rate (avg bytes per second)
			               0,    0, // data block size in bytes
			               0,    0, // bits per sample
			0x64, 0x61, 0x74, 0x61, // "data"
			   0,    0,    0,    0, // size of the data section
	};

	private static byte[] HEADER_TEMPLATE_NON_PCM = new byte[] {
			0x52, 0x49, 0x46, 0x46,
			   0,    0,    0,    0,
			0x57, 0x41, 0x56, 0x45,
			0x66, 0x6D, 0x74, 0x20,
			0x12,    0,    0,    0,
			             0x3,    0,
			               0,    0,
			   0,    0,    0,    0,
			   0,    0,    0,    0,
			               0,    0,
			               0,    0,
			               0,    0, // extension size
			0x66, 0x61, 0x63, 0x74, // "fact"
			   0,    0,    0,    0, // size of fact section
			0x64, 0x61, 0x74, 0x61,
			   0,    0,    0,    0,
	};

	private byte[] header;

	private void setHeader() {
		header = (encodingType == AudioFormat.ENCODING_PCM_FLOAT)
					? HEADER_TEMPLATE_NON_PCM
					: HEADER_TEMPLATE_PCM;
		byte channels;
		if (inputType == AudioFormat.CHANNEL_IN_STEREO) {
			channels = 2;
		} else {
			channels = 1;
		}
		header[22] = channels;

		switch (encodingType) {
			case AudioFormat.ENCODING_PCM_16BIT:
				bytesPerSample = 2;
				break;
			case AudioFormat.ENCODING_PCM_8BIT:
				bytesPerSample = 1;
				break;
			case AudioFormat.ENCODING_PCM_FLOAT:
				bytesPerSample = 4;
				break;
			default:
				return;
		}

		for (int i = 24, j = 0; i <= 27; i++, j += 8) {
			header[i] = (byte) (sampleRate >> j);
		}

		int byteRate = sampleRate * bytesPerSample * channels;
		for (int i = 28, j = 0; i <= 31; i++, j += 8) {
			header[i] = (byte) (byteRate >> j);
		}
		header[32] = (byte) (bytesPerSample * channels);
		header[34] = (byte) (bytesPerSample * 8);
	}

	public WaveConfig(int inputType, int sampleRate, int encodingType) {
		this.inputType = inputType;
		this.sampleRate = sampleRate;
		this.encodingType = encodingType;

		setHeader();
	}

	public int getInputType() {
		return inputType;
	}

	public int getSampleRate() {
		return sampleRate;
	}

	public int getEncodingType() {
		return encodingType;
	}

	public byte[] getHeaderByteArray() {
		return header;
	}

	public void completeHeaderData(File file) throws IOException {
		RandomAccessFile raf = new RandomAccessFile(file, "rw");
		int fileLength = (int) raf.length();
		int dataLength = fileLength - header.length;
		raf.seek(4);
		raf.writeInt(Utils.toLittleEndian(fileLength));
		if (header.length == HEADER_TEMPLATE_NON_PCM.length) {
			raf.seek(header.length - 12);
			raf.writeInt(Utils.toLittleEndian(dataLength / bytesPerSample));
		}
		raf.seek(header.length - 4);
		raf.writeInt(Utils.toLittleEndian(dataLength));
		raf.close();
	}
}
