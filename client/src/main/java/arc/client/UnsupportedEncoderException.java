package arc.client;

public class UnsupportedEncoderException extends Exception {
	public UnsupportedEncoderException() {
		super("The encoder is not supported");
	}
}
