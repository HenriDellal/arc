package arc.client;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

public class ServerFragment extends Fragment implements View.OnClickListener {

	public ServerFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_server, container, false);
		view.findViewById(R.id.btn_connect).setOnClickListener(this);
		TextView status = (TextView) view.findViewById(R.id.text_status);
		status.setText(getInitialStatusText(
				((MainActivity)getActivity())
						.getTCPService()
		));
		return view;
	}

	@Override
	public void onClick(View view) {
		MainActivity mainActivity = (MainActivity) getActivity();
		TCPClientService service = mainActivity.getTCPService();
		switch (view.getId()) {
			case R.id.btn_connect:
				Button b = (Button) view;
				b.setEnabled(false);
				if (service.isRunning()) {
					service.stop();
				} else {
					SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
					String address = preferences.getString("server_address", null);
					int port = Integer.parseInt(preferences.getString("server_port", "8080"));

					service.start(mainActivity, address, port);
				}
				break;
		}
	}

	private String getInitialStatusText(TCPClientService service) {
		Resources res = getResources();
		String template;
		if (service.isRunning()) {
			template = res.getString(R.string.connected_to);
			return String.format(template, service.getAddress());
		} else if (!isWifiEnabled()) {
			return res.getString(R.string.wifi_is_not_enabled);
		} else {
			template = res.getString(R.string.not_connected);
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
			String address = preferences.getString("server_address", null);
			String port = preferences.getString("server_port", null);
			return String.format(template, address, port);
		}
	}

	public void onClientStateChange(int clientState) {
		TextView status = (TextView) getView().findViewById(R.id.text_status);
		status.setText(getInitialStatusText(
				((MainActivity)getActivity())
						.getTCPService()
		));
		Button connectButton = (Button) getView().findViewById(R.id.btn_connect);
		connectButton.setEnabled(true);
		Resources res = getResources();
		switch (clientState) {
			case ClientState.CONNECTION_STARTED:
				connectButton.setText(res.getString(R.string.disconnect));
				break;
			case ClientState.CONNECTION_CLOSED:
				connectButton.setText(res.getString(R.string.connect));
				break;
		}
	}

	@SuppressWarnings("deprecation")
	private boolean isWifiEnabled() {
		ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		if (null == cm) {
			return false;
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			Network network = cm.getActiveNetwork();
			NetworkCapabilities nc = cm.getNetworkCapabilities(network);
			return null != nc && nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
		} else {
			NetworkInfo networkInfo = cm.getActiveNetworkInfo();
			return networkInfo != null && networkInfo.isConnected()
					&& networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
		}
	}
}