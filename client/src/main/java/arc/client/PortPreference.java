package arc.client;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;

public class PortPreference extends EditTextPreference
		implements EditTextPreference.OnBindEditTextListener, Preference.OnPreferenceChangeListener {

	public PortPreference(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		setOnPreferenceChangeListener(this);
		setOnBindEditTextListener(this);
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		String s = (String) newValue;
		int port;
		try {
			port = Integer.parseInt(s);
			if (port >= 1024 && port < 65536) {
				return true;
			}
		} catch (NumberFormatException e) {
			return false;
		}
		return false;
	}

	@Override
	public void onBindEditText(@NonNull EditText editText) {
		editText.setInputType(InputType.TYPE_CLASS_NUMBER);
	}
}
