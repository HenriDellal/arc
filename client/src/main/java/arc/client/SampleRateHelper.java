package arc.client;

import android.media.AudioRecord;

public class SampleRateHelper {
	public static int[] SAMPLE_RATES = new int[] {
		8000, 11025, 16000, 22050, 44100, 48000
	};
	public static CharSequence[] SAMPLE_RATES_STRINGS = new CharSequence[] {
			"8000", "11025", "16000", "22050", "44100", "48000"
	};

	public static CharSequence[] getSampleRates(int inputType, int encoderType) {
		boolean[] isSampleRateSupported = new boolean[SAMPLE_RATES.length];
		int resultLength = 0;
		for (int i = 0; i < SAMPLE_RATES.length; i++) {
			int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATES[i], inputType, encoderType);
			if (bufferSize > 0) {
				isSampleRateSupported[i] = true;
				resultLength++;
			}
		}

		CharSequence[] result = new CharSequence[resultLength];

		for (int i = 0, j = 0; i < isSampleRateSupported.length; i++) {
			if (isSampleRateSupported[i]) {
				result[j] = SAMPLE_RATES_STRINGS[i];
				j++;
			}
		}
		return result;
	}
}
