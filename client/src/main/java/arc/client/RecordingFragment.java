package arc.client;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.io.File;
import java.io.IOException;

public class RecordingFragment extends Fragment implements View.OnClickListener {

	private RecordingService getRecordingService() {
		return ((MainActivity)getActivity()).getService();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.start_recording:
			case R.id.btn_stop:
				getRecordingService().toggleRecording((MainActivity)getActivity());
				updateRecordingControls();
				break;
			case R.id.btn_pause:
				getRecordingService().togglePause((MainActivity)getActivity());
				updatePauseButton();
				break;
			case R.id.btn_open_directory:
				openRecordingsDir();
				break;
		}
	}

	private void openRecordingsDir() {
		File recordingsDir = getContext().getExternalFilesDir(Environment.DIRECTORY_MUSIC);
		if (!recordingsDir.exists()) {
			recordingsDir.mkdirs();
		}
		Intent intent = new Intent(Intent.ACTION_VIEW);
		String path = "";
		try {
			path = recordingsDir.getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Uri uri = Uri.parse(path);
		intent.setDataAndType(uri, "resource/folder");
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		if (null != intent.resolveActivityInfo(getContext().getPackageManager(), 0)) {
			startActivity(intent);
		} else {
			Toast.makeText(getContext(), R.string.msg_file_manager_missing, Toast.LENGTH_LONG).show();
		}
	}

	private static final int[] buttonIds = new int[] {
			R.id.start_recording,
			R.id.btn_open_directory,
			R.id.btn_pause,
			R.id.btn_stop
	};

	public RecordingFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_recording, container, false);
		for (int i = 0; i < buttonIds.length; i++)
			view.findViewById(buttonIds[i]).setOnClickListener(this);
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		updateRecordingControls();
		updatePauseButton();
	}

	public void updateRecordingControls() {
		Recorder recorder;
		if (null != getRecordingService() && null != (recorder = getRecordingService().getRecorder())) {
			View recordButton = getView().findViewById(R.id.start_recording);
			View recordControls = getView().findViewById(R.id.record_controls);
			if (recorder.isRecording()) {
				recordControls.setVisibility(View.VISIBLE);
				recordButton.setVisibility(View.GONE);
			} else {
				recordControls.setVisibility(View.GONE);
				recordButton.setVisibility(View.VISIBLE);
			}
		}
	}

	public void updatePauseButton() {
		ImageButton pauseButton = (ImageButton) getView().findViewById(R.id.btn_pause);
		Recorder recorder;
		if (null != getRecordingService() && null != (recorder = getRecordingService().getRecorder())) {
			pauseButton.setImageDrawable(getResources().getDrawable(recorder.isPaused() ?
					R.drawable.ic_resume :
					R.drawable.ic_pause, null));
		}
	}
}