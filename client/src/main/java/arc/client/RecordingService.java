package arc.client;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.preference.PreferenceManager;

import arc.message.MessageFactory;
import arc.message.NotifyMessage;
import arc.message.Prefix;

public class RecordingService extends Service {
	private final IBinder binder = new RecordBinder();
	private Recorder recorder;

	private boolean updateRecorderPrefs;

	public void updateRecorderPrefs() {
		updateRecorderPrefs = true;
	}

	public static final int WAV = 0;
	@Override
	public void onCreate() {
		super.onCreate();
		updateRecorder();
	}

	private void updateRecorder() {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String outputType = preferences.getString("audio_format", EncoderHelper.OUTPUT_TYPE_CUSTOM_WAV);
		if (EncoderHelper.OUTPUT_TYPE_CUSTOM_WAV.equals(outputType)) {
			recorder = new AudioRecorder(getApplicationContext());
		} else {
			recorder = new MultiformatRecorder();
		}
		updateRecorderPrefs = false;
	}

	public Recorder getRecorder() {
		return recorder;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	public void toggleRecording(MainActivity activity) {
		String cmd;
		int state;
		if (recorder.isRecording()) {
			recorder.stop();
			cmd = Prefix.NOTIFY_STOP_RECORDING;
			state = Prefix.STATE_STOP_RECORDING;
			Toast.makeText(activity, String.format(getResources().getString(R.string.msg_file_saved_as), recorder.getFilename()), Toast.LENGTH_SHORT).show();
		} else {
			if (updateRecorderPrefs)
				updateRecorder();
			recorder.start(activity);
			cmd = Prefix.NOTIFY_START_RECORDING;
			state = Prefix.STATE_START_RECORDING;
		}
		notifyServer(activity, cmd, state);
	}

	@Override
	public void onDestroy() {
		recorder.stop();
		super.onDestroy();
	}

	public boolean hasPlannedRecorderUpdate() {
		return updateRecorderPrefs;
	}

	public void togglePause(MainActivity activity) {
		String cmd;
		int state;
		if (recorder.isPaused()) {
			recorder.resume();
			cmd = Prefix.NOTIFY_START_RECORDING;
			state = Prefix.STATE_START_RECORDING;
		} else {
			recorder.pause();
			cmd = Prefix.NOTIFY_PAUSE_RECORDING;
			state = Prefix.STATE_PAUSE_RECORDING;
		}
		notifyServer(activity, cmd, state);
	}

	public NotifyMessage getNotificationMessage(String name) {
		String cmd;
		int state;
		if (recorder.isPaused()) {
			cmd = Prefix.NOTIFY_PAUSE_RECORDING;
			state = Prefix.STATE_PAUSE_RECORDING;
		} else if (recorder.isRecording()) {
			cmd = Prefix.NOTIFY_START_RECORDING;
			state = Prefix.STATE_START_RECORDING;
		} else {
			cmd = Prefix.NOTIFY_STOP_RECORDING;
			state = Prefix.STATE_STOP_RECORDING;
		}
		NotifyMessage nm = MessageFactory.formNotifyMessage(
				Prefix.CLIENT_MAGIC,
				name,
				cmd,
				state);
		return nm;
	}

	private void notifyServer(MainActivity activity, String cmd, int state) {
		TCPClientService.TCPOutputWorker outputWorker = activity.getTCPService().getOutputWorker();
		if (null != outputWorker) {
			NotifyMessage nm = MessageFactory.formNotifyMessage(
					Prefix.CLIENT_MAGIC,
					activity.getTCPService().getName(),
					cmd,
					state
			);
			outputWorker.sendMessage(nm);
		}
	}

	public class RecordBinder extends Binder {
		RecordingService getService() {
			return RecordingService.this;
		}
	}
}
