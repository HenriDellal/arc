package arc.client;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.preference.PreferenceManager;

import com.google.android.material.navigation.NavigationView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
	private final String[] permissions = new String[] {Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE};
	private NavController navController;
	private AppBarConfiguration abc;

	private RecordingService service;
	private TCPClientService tcpClientService;

	public RecordingService getService() {
		return service;
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	private Intent recordingIntent, tcpClientIntent;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		PreferenceManager.setDefaultValues(getBaseContext(), R.xml.root_preferences, false);
		recordingIntent = new Intent(this, RecordingService.class);
		tcpClientIntent = new Intent(this, TCPClientService.class);
		if (Build.VERSION.SDK_INT >= 23) {
			for (String permission: permissions) {
				if (checkSelfPermission(permission) == PackageManager.PERMISSION_DENIED) {
					requestPermissions(permissions, 0);
					break;
				}
			}
		}

		Timer timer = new Timer();
		timer.schedule(timerUpdater, 0, 250);
		final DrawerLayout drawerLayout = findViewById(R.id.main_navigation_view);
		Toolbar toolbar = (Toolbar)findViewById(R.id.main_toolbar);
		setSupportActionBar(toolbar);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				drawerLayout.open();
			}
		});

		NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
		navController = navHostFragment.getNavController();
		abc =
				new AppBarConfiguration.Builder(navController.getGraph())
						.setOpenableLayout(drawerLayout)
						.build();
		NavigationUI.setupActionBarWithNavController(this, navController, abc);
		NavigationUI.setupWithNavController((NavigationView) findViewById(R.id.nav_view), navController);

	}

	@Override
	protected void onStart() {
		super.onStart();
		bindService(recordingIntent, connection, BIND_AUTO_CREATE);
		bindService(tcpClientIntent, clientServiceConnection, BIND_AUTO_CREATE);
	}

	@Override
	protected void onDestroy() {
		unbindService(connection);
		unbindService(clientServiceConnection);
		super.onDestroy();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		for (int i : grantResults) {
			if (i == PackageManager.PERMISSION_DENIED) {
				finish();
			}
		}
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}

	private ServiceConnection connection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
			RecordingService.RecordBinder binder = (RecordingService.RecordBinder) iBinder;
			service = binder.getService();
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {

		}
	};

	private ServiceConnection clientServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
			TCPClientService.ClientBinder binder = (TCPClientService.ClientBinder) iBinder;
			tcpClientService = binder.getService();
			String clientName = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
					.getString("client_name", null);
			if (null == clientName) {
				clientName = Utils.generateClientName();
				PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
						.edit().putString("client_name", clientName).apply();
			}

			tcpClientService.setName(clientName);
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {

		}
	};

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		return NavigationUI.onNavDestinationSelected(item, navController)
				|| super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onSupportNavigateUp() {
		return navController.navigateUp() || super.onSupportNavigateUp();
	}

	private TimerTask timerUpdater = new TimerTask() {
		public void run() {
			final View view = findViewById(R.id.text_time);
			if (null != view && null != service && null != service.getRecorder()) {
				long seconds = (long) service.getRecorder().getDuration() / 1000l;
				final String time = String.format("%d:%02d", seconds / 60, seconds % 60);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						((TextView)view).setText(time);
					}
				});
			}
		}
	};

	public TCPClientService getTCPService() {
		return tcpClientService;
	}

	private Fragment getCurrentFragment() {
		NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
		return navHostFragment.getChildFragmentManager().getFragments().get(0);
	}

	public void updateRecordingControls() {
		Fragment fragment = getCurrentFragment();
		if (null != fragment) {
			if (!(fragment instanceof RecordingFragment)) {
				return;
			}
			RecordingFragment rf = (RecordingFragment) fragment;
			rf.updateRecordingControls();
			rf.updatePauseButton();
		} else {
			Log.e("arc.client", "fragment is null");
		}
	}

	public void notifyClientStateChange(int clientState) {
		Fragment fragment = getCurrentFragment();
		if (null != fragment && fragment instanceof ServerFragment) {
			((ServerFragment) fragment).onClientStateChange(clientState);
		}
	}
}