package arc.client;

import android.media.MediaCodecInfo;

public class Encoder {
	private String type;
	private String name;
	private int profiles[];

	public Encoder(MediaCodecInfo info, String type) throws UnsupportedEncoderException {
		this.type = type;
		name = info.getName();
		if (!EncoderHelper.isValidData(name, type)) {
			throw new UnsupportedEncoderException();
		}

		MediaCodecInfo.CodecCapabilities mediaCapabilities = info.getCapabilitiesForType(type);
		MediaCodecInfo.CodecProfileLevel[] levels =
				mediaCapabilities.profileLevels;
		if (levels.length > 0) {
			profiles = new int[levels.length];
			for (int i = 0; i < profiles.length; i++) {
				profiles[i] = levels[i].profile;
			}
		}
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}
}
