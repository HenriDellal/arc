package arc.client;

import android.content.Context;
import android.util.AttributeSet;

import androidx.preference.EditTextPreference;
import androidx.preference.Preference;

import java.util.regex.Pattern;

public class ClientNamePreference extends EditTextPreference
		implements Preference.OnPreferenceChangeListener {

	private boolean isValidName(String name) {
		return Pattern.matches("[\\S ]{1,32}", name);
	}

	public ClientNamePreference(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		setOnPreferenceChangeListener(this);
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		String name = (String) newValue;
		return isValidName(name);
	}
}
