package arc.client;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerAddressPreference extends EditTextPreference
		implements EditTextPreference.OnBindEditTextListener, Preference.OnPreferenceChangeListener {
	private static final String IPv4_REGEX =
			"^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
			"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
			"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
			"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";

	private static final Pattern IPv4_PATTERN = Pattern.compile(IPv4_REGEX);

	private boolean isValidAddress(String address) {
		Matcher matcher = IPv4_PATTERN.matcher(address);
		return matcher.matches();
	}

	public ServerAddressPreference(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		setOnPreferenceChangeListener(this);
		setOnBindEditTextListener(this);
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		String address = (String) newValue;
		return isValidAddress(address);
	}

	@Override
	public void onBindEditText(@NonNull EditText editText) {
		editText.setInputType(InputType.TYPE_CLASS_PHONE);
	}
}
