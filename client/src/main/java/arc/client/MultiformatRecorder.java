package arc.client;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.preference.PreferenceManager;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class MultiformatRecorder implements Recorder {
	private MediaRecorder recorder;
	private File file;
	private Timer timer;

	private float duration;
	private boolean isPaused;

	public MultiformatRecorder() {}

	@Override
	public void start(Context context) {
		isPaused = false;
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		int source = Integer.parseInt(preferences.getString("audio_source_type", String.valueOf(MediaRecorder.AudioSource.VOICE_RECOGNITION)));
		int outputFormat, encoder;

		String outputFormatStr = preferences.getString("audio_format", "");
		switch (outputFormatStr) {
			case EncoderHelper.OUTPUT_TYPE_3GPP:
				outputFormat = MediaRecorder.OutputFormat.THREE_GPP;
				encoder = MediaRecorder.AudioEncoder.AMR_NB;
				break;
			case EncoderHelper.OUTPUT_TYPE_AMR_WB:
				outputFormat = MediaRecorder.OutputFormat.AMR_WB;
				encoder = MediaRecorder.AudioEncoder.AMR_WB;
				break;
			case EncoderHelper.OUTPUT_TYPE_MP4:
				outputFormat = MediaRecorder.OutputFormat.MPEG_4;
				encoder = MediaRecorder.AudioEncoder.AAC;
				break;
			default:
				outputFormat = MediaRecorder.OutputFormat.DEFAULT;
				encoder = MediaRecorder.AudioEncoder.DEFAULT;
		}
		recorder = new MediaRecorder();
		recorder.setAudioSource(source);
		recorder.setOutputFormat(outputFormat);
		recorder.setAudioEncoder(encoder);

		file = Utils.getRecordingFile(context, Utils.getFileExtension(outputFormat));
		recorder.setOutputFile(file.getPath());
		try {
			recorder.prepare();
		} catch (IOException e) {
			e.printStackTrace();
		}
		recorder.start();
		duration = 0.0f;
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				if (!isPaused)
					duration += 50.f;
			}
		}, 0, 50);
	}

	@RequiresApi(api = Build.VERSION_CODES.N)
	@Override
	public void pause() {
		isPaused = true;
		recorder.pause();
	}

	@RequiresApi(api = Build.VERSION_CODES.N)
	@Override
	public void resume() {
		isPaused = false;
		recorder.resume();
	}

	public float getDuration() {
		return duration;
	}
	@Override
	public void stop() {
		if (null != recorder) {
			recorder.stop();
			recorder.reset();
			recorder.release();
		}
		if (null != timer)
			timer.cancel();
		recorder = null;
	}

	@Override
	public boolean isRecording() {
		return null != recorder;
	}

	@Override
	public String getFilename() {
		return file.getAbsolutePath();
	}

	@Override
	public boolean isPaused() {
		return isPaused;
	}
}
