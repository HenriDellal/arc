package arc.client;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.AutomaticGainControl;
import android.media.audiofx.NoiseSuppressor;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.preference.PreferenceManager;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class AudioRecorder implements Recorder {
	private AcousticEchoCanceler aec;
	private AudioRecord record;
	private AutomaticGainControl agc;
	private File file;
	private FileOutputStream fos;
	private NoiseSuppressor noiseSuppressor;
	private WaveConfig waveConfig;

	private int minBufferSize;
	private boolean isPaused;
	private float duration;
	private int source;

	public AudioRecorder(Context context) {
		int inputType, encodingType, sampleRate;
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		inputType = Integer.parseInt(
				preferences.getString("input_channel_type",
						String.valueOf(AudioFormat.CHANNEL_IN_MONO)));
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			encodingType = Integer.parseInt(
					preferences.getString("encoding_type",
							String.valueOf(AudioFormat.ENCODING_PCM_16BIT)));
		} else {
			encodingType = AudioFormat.ENCODING_PCM_16BIT;
		}

		sampleRate = Integer.parseInt(preferences.getString("sample_rate_wav", "44100"));

		waveConfig = new WaveConfig(inputType, sampleRate, encodingType);
		source = Integer.parseInt(
				preferences.getString("audio_source_type",
						String.valueOf(MediaRecorder.AudioSource.VOICE_RECOGNITION)));
	}

	public void start(Context context) {
		int sampleRateInHz = waveConfig.getSampleRate();
		int channelConfig = waveConfig.getInputType();
		int encodingType = waveConfig.getEncodingType();
		minBufferSize = AudioRecord.getMinBufferSize(sampleRateInHz, channelConfig, encodingType);
		Log.e("arc.client", Integer.toString(minBufferSize));
		record = new AudioRecord(
				source,
				sampleRateInHz,
				channelConfig,
				encodingType,
				minBufferSize);
		int sessionId = record.getAudioSessionId();

		if (NoiseSuppressor.isAvailable()) {
			noiseSuppressor = NoiseSuppressor.create(sessionId);
			if (null != noiseSuppressor && !noiseSuppressor.getEnabled())
				noiseSuppressor.setEnabled(true);
		}

		if (AutomaticGainControl.isAvailable()) {
			agc = AutomaticGainControl.create(sessionId);
			if (agc != null && !agc.getEnabled())
				agc.setEnabled(true);
		}

		if (AcousticEchoCanceler.isAvailable()) {
			aec = AcousticEchoCanceler.create(sessionId);
			if (null != aec && !aec.getEnabled()) {
				aec.setEnabled(true);
			}
		}

		record.startRecording();

		file = Utils.getRecordingFile(context, "wav");
		new Thread(new AudioRecorderRunnable()).start();
	}

	@Override
	public void pause() {
		isPaused = true;
	}

	@Override
	public void resume() {
		isPaused = false;
	}

	@Override
	public float getDuration() {
		return duration;
	}

	@Override
	public String getFilename() {
		return file.getAbsolutePath();
	}

	@Override
	public boolean isPaused() {
		return isPaused;
	}

	public void stop() {
		if (null != record) {
			record.stop();
			record.release();
			record = null;
		}
		if (null != noiseSuppressor) {
			noiseSuppressor.release();
			noiseSuppressor = null;
		}
		if (null != agc) {
			agc.release();
			agc = null;
		}
		if (null != aec) {
			aec.release();
			aec = null;
		}
		duration = 0.0f;
	}

	public boolean isRecording() {
		return null != record && record.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING;
	}

	private class AudioRecorderRunnable implements Runnable {

		@SuppressLint("NewApi")
		@Override
		public void run() {
			try {
				fos = new FileOutputStream(file, true);
				BufferedOutputStream bos = new BufferedOutputStream(fos);
				DataOutputStream dos = new DataOutputStream(bos);
				dos.write(waveConfig.getHeaderByteArray());
				if (waveConfig.getEncodingType() == AudioFormat.ENCODING_PCM_FLOAT) {
					record(new float[minBufferSize / 4], dos);
				} else {
					record(new short[minBufferSize / 2], dos);
				}
				dos.flush();
				dos.close();
				waveConfig.completeHeaderData(file);
			} catch (IOException e) {
				e.printStackTrace();
				stop();
			}
		}

		private void record(short[] shortData, DataOutputStream dos) throws IOException {
			short[] data = shortData;
			duration = 0.0f;
			long startPoint = System.currentTimeMillis();
			long newStartPoint;
			while (record.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
				int dataSize = record.read(data, 0, data.length);
				newStartPoint = System.currentTimeMillis();
				if (!isPaused) {
					data = getHighPassFilteredData(getLowPassFilteredData(data, dataSize), dataSize);
					for (int i = 0; i < dataSize; i++) {
						dos.writeShort(data[i] >> 2);
					}
					duration += newStartPoint - startPoint;
				}
				startPoint = newStartPoint;
			}
		}

		@RequiresApi(api = Build.VERSION_CODES.M)
		private void record(float[] floatData, DataOutputStream dos) throws IOException {
			float[] data = floatData;
			duration = 0.0f;
			long startPoint = System.currentTimeMillis();
			long newStartPoint;
			while (record.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
				int dataSize = record.read(data, 0, data.length, AudioRecord.READ_NON_BLOCKING);
				newStartPoint = System.currentTimeMillis();
				if (!isPaused) {
					data = getHighPassFilteredData(getLowPassFilteredData(data, dataSize), dataSize);
					for (int i = 0; i < dataSize; i++) {
						dos.writeFloat(data[i]);
					}
					duration += newStartPoint - startPoint;
				}
				startPoint = newStartPoint;
			}
		}

		private short[] getHighPassFilteredData(short[] data, int dataSize) {
			short[] result = new short[data.length];
			float alpha = 0.5f;
			result[0] = data[0];
			for (int i = 1; i < dataSize; i++) {
				result[i] = (short) (alpha * result[i-1] + alpha * (data[i] - data[i-1]));
			}
			return result;
		}

		private short[] getLowPassFilteredData(short[] data, int dataSize) {
			short[] result = new short[data.length];
			float alpha = 0.3f;
			result[0] = (short) (alpha * data[0]);
			for (int i = 1; i < dataSize; i++) {
				result[i] = (short) (alpha * data[i] + (1-alpha) * result[i-1]);
			}
			return result;
		}

		private float[] getHighPassFilteredData(float[] data, int dataSize) {
			float[] result = new float[data.length];
			float alpha = 0.5f;
			result[0] = data[0];
			for (int i = 1; i < dataSize; i++) {
				result[i] = alpha * result[i-1] + alpha * (data[i] - data[i-1]);
			}
			return result;
		}

		private float[] getLowPassFilteredData(float[] data, int dataSize) {
			float[] result = new float[data.length];
			float alpha = 0.3f;
			result[0] = alpha * data[0];
			for (int i = 1; i < dataSize; i++) {
				result[i] = alpha * data[i] + (1-alpha) * result[i-1];
			}
			return result;
		}
	}
}
