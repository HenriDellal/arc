package arc.client;

import android.media.MediaCodecInfo;
import android.media.MediaCodecList;

import java.util.ArrayList;

public class EncoderHelper {
	public static final String AAC_ENCODER_NAME = "OMX.google.aac.encoder";
	public static final String AMR_NB_ENCODER_NAME = "OMX.google.amrnb.encoder";
	public static final String AMR_WB_ENCODER_NAME = "OMX.google.amrwb.encoder";

	public static final String[] AVAILABLE_ENCODERS_NAMES = new String[] {
			AAC_ENCODER_NAME,
			AMR_NB_ENCODER_NAME,
			AMR_WB_ENCODER_NAME
	};

	public static final String OUTPUT_TYPE_MP4 = "audio/mp4a-latm";
	public static final String OUTPUT_TYPE_3GPP = "audio/3gpp";
	public static final String OUTPUT_TYPE_AMR_WB = "audio/amr-wb";
	public static final String OUTPUT_TYPE_CUSTOM_WAV = "audio/custom-wav";

	public static final String[] AVAILABLE_ENCODERS_TYPES = new String[] {
			OUTPUT_TYPE_MP4,
			OUTPUT_TYPE_3GPP,
			OUTPUT_TYPE_AMR_WB
	};

	public static boolean isValidData(String name, String type) {
		boolean isValidName = false, isValidType = false;
		for (String availableName: AVAILABLE_ENCODERS_NAMES) {
			if (availableName.equals(name)) {
				isValidName = true;
				break;
			}
		}
		for (String availableType: AVAILABLE_ENCODERS_TYPES) {
			if (availableType.equals(type)) {
				isValidType = true;
				break;
			}
		}
		return isValidName && isValidType;
	}

	private static Encoder[] encoders;

	public static Encoder[] getEncoders() {
		if (encoders != null)
			return encoders;

		MediaCodecInfo[] infos = new MediaCodecList(MediaCodecList.ALL_CODECS).getCodecInfos();
		ArrayList<Encoder> encodersList = new ArrayList<>();
		for (MediaCodecInfo info: infos) {
			if (info.isEncoder()) {
				String[] types = info.getSupportedTypes();

				for (String type : types) {
					if (!type.startsWith("audio"))
						continue;

					Encoder encoder = null;
					try {
						encoder = new Encoder(info, type);
					} catch (UnsupportedEncoderException e) {
						e.printStackTrace();
					}
					if (encoder != null)
						encodersList.add(encoder);
				}
			}
		}
		encoders = new Encoder[encodersList.size()];
		encodersList.toArray(encoders);
		return encoders;
	}
}
