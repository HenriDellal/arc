package arc.message;

public class NameChangeMessage extends NotifyMessage {
	private String newName;

	public static int DATA_COUNT = 3;

	@Override
	public void setData(String data) {
		String[] dataParts = data.split(Prefix.SEPARATOR_STRING, DATA_COUNT);
		state = Integer.parseInt(dataParts[0]);
		name = dataParts[1];
		newName = dataParts[2];
	}

	@Override
	public String getData() {
		StringBuilder sb = new StringBuilder();
		sb.append(state)
				.append(Prefix.SEPARATOR_STRING)
				.append(name)
				.append(Prefix.SEPARATOR_STRING)
				.append(newName);
		return sb.toString();
	}

	public String getNewName() {
		return newName;
	}
}
