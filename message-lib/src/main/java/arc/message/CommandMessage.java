package arc.message;

public class CommandMessage extends Message {
	private String data;

	@Override
	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String getData() {
		return data;
	}
}
