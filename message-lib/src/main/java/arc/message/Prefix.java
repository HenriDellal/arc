package arc.message;

public class Prefix {
	public static final String SERVER_MAGIC = "arcserver";
	public static final String CLIENT_MAGIC = "arcclient";
	public static final byte PROTOCOL_VERSION = 2;

	public static final String INIT_MESSAGE = "hello";
	public static final String CMD_SET_NAME = "setname";
	public static final String NOTIFY_NAME_CHANGE = "namechange";
	public static final int NOTIFY_NAME_CHANGE_STATE = 4;

	public static final String CMD_START_RECORDING = "startrec";
	public static final int UNKNOWN_STATE = 0;
	public static final String NOTIFY_START_RECORDING = "notifystart";
	public static final int STATE_START_RECORDING = 1;

	public static final String CMD_PAUSE_RECORDING = "pauserec";
	public static final String NOTIFY_PAUSE_RECORDING = "notifypause";
	public static final int STATE_PAUSE_RECORDING = 2;

	public static final String NOTIFY_STOP_RECORDING = "notifystop";
	public static final String CMD_STOP_RECORDING = "stoprec";
	public static final int STATE_STOP_RECORDING = 3;

	public static final String INFO_RECORD_STATS = "recordstats";

	public static final String CMD_GET_RECORDING_STATUS = "recstatus";


	public static final String SEPARATOR_STRING = "@";
	/*
	Template 1

	SERVER_MAGIC +
	PROTOCOL_VERSION +
	INIT_MESSAGE +
	serverName.size() +
	serverName
	 */

	/*
	Template 2
	CLIENT_MAGIC +
	PROTOCOL_VERSION +
	INIT_MESSAGE +
	clientName.size() +
	clientName
	 */

	/*
	Template 3
	SERVER_MAGIC +
	PROTOCOL_VERSION +
	CMD_START_RECORDING
	 */
}
