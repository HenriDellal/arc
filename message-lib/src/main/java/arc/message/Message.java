package arc.message;

public abstract class Message {
	protected String magic;
	protected String cmd;

	protected String name;

	public Message() {}

	public abstract void setData(String data);

	public void setMagic(String magic) {
		this.magic = magic;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getMagic() {
		return magic;
	}

	public String getCmd() {
		return cmd;
	}
	public abstract String getData();

	public String getName() {
		return name;
	}
}
