package arc.message;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class MessageFactory {
	public static final int BUFFER_SIZE = 1024;

	public static Message create(BufferedReader br) throws IOException {
		char[] data = new char[BUFFER_SIZE];
		System.out.println("start of creation");
		assert br != null;
		System.out.println("inputstream is not null");

		Message result = null;

		StringBuilder sb = new StringBuilder();
		int maxMagicLength = MessageSpec.MAGIC_SIZE;
		char[] magicData = new char[maxMagicLength];
		int i = 0;
		while (i < maxMagicLength-1) {
			int c = br.read(magicData, 0, MessageSpec.MAGIC_SIZE-i);
			if (c == -1) {
				break;
			}
			i += c;
			sb.append(magicData);
		}
		System.out.println("Magic: " + sb.toString());

		int count = br.read(data, 0, MessageSpec.PROTOCOL_VERSION_SIZE);
		char protocolVersion = data[0];
		if (protocolVersion > Prefix.PROTOCOL_VERSION) {
			System.out.println("Protocol version: " + protocolVersion);
			throw new IOException(String.format("The sender's protocol version is newer than yours %d, update the application to fix this error.", protocolVersion));
		}

		i = 0;
		StringBuilder cmd = new StringBuilder();
		while (i < (MessageSpec.CMD_SIZE - 1)) {
			i += br.read(data, 0, MessageSpec.CMD_SIZE);
			if (i == -1) {
				return null;
			}
			cmd.append(Arrays.copyOfRange(data, 0, i));
		}

		String cmdStr = cmd.toString().trim();
		System.out.println("cmd: " + cmdStr);
		Class<? extends Message> resultClass = null;
		switch (cmdStr) {
			case Prefix.CMD_SET_NAME:
			case Prefix.CMD_START_RECORDING:
			case Prefix.CMD_PAUSE_RECORDING:
			case Prefix.CMD_GET_RECORDING_STATUS:
			case Prefix.CMD_STOP_RECORDING:
				resultClass = CommandMessage.class;
				break;
			case Prefix.INIT_MESSAGE:
				resultClass = InitMessage.class;
				break;
			case Prefix.NOTIFY_NAME_CHANGE:
				resultClass = NameChangeMessage.class;
				break;
			case Prefix.NOTIFY_START_RECORDING:
			case Prefix.NOTIFY_PAUSE_RECORDING:
			case Prefix.NOTIFY_STOP_RECORDING:
				resultClass = NotifyMessage.class;
				break;
			case Prefix.INFO_RECORD_STATS:
				resultClass = StatsMessage.class;
				break;
			default:
				throw new IOException(String.format("Unknown type of message: %s", cmd));
		}

		int cmdDataSize = 0;
		for (int j = 0; j < MessageSpec.CMD_DATA_SIZE_SIZE; ){
			char[] temp = new char[1];
			int bytes = br.read(temp, 0, 1);
			if (bytes > 0) {
				cmdDataSize += temp[0] * Math.pow(256, MessageSpec.CMD_DATA_SIZE_SIZE - 1 - j);
				j += bytes;
			}
		}
		System.out.println(cmdDataSize);

		i = 0;
		StringBuilder cmdData = new StringBuilder();
		if (cmdDataSize > 0) {
			cmdDataSize = Math.min(cmdDataSize, MessageSpec.MAX_CMD_DATA_SIZE);
			while (i < cmdDataSize-1) {
				count = br.read(data, 0, cmdDataSize);
				i += count;
				cmdData.append(Arrays.copyOfRange(data, 0, count));
			}
		}
		System.out.println("Command data: " + cmdData.toString());

		try {
			result = resultClass.newInstance();

			result.setMagic(sb.toString());
			result.setCmd(cmdStr);
			result.setData(cmdData.toString());
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}

		return result;
	}


	public static void send(DataOutputStream dos, Message message) throws IOException {
		dos.write(message.getMagic().getBytes());
		dos.writeByte(Prefix.PROTOCOL_VERSION);
		byte[] cmd = message.getCmd().getBytes();
		dos.write(cmd);

		if (cmd.length < MessageSpec.CMD_SIZE) {
			dos.write(new byte[MessageSpec.CMD_SIZE - cmd.length]);
		}

		String data = message.getData();
		if (null == data) {
			dos.writeByte(0);
			dos.writeByte(0);
		} else {
			byte[] dataBytes = data.getBytes();
			dos.writeByte(dataBytes.length / 256);
			dos.writeByte(dataBytes.length % 256);
			dos.write(dataBytes);
		}
	}

	public static InitMessage formInitMessage(String magic, String name) {
		InitMessage result = new InitMessage();
		result.setMagic(magic);
		result.setCmd(Prefix.INIT_MESSAGE);
		result.setData(name);
		return result;
	}

	public static StatsMessage formStatsMessage(String magic, String name, String cmd) {
		StatsMessage result = new StatsMessage();
		result.setMagic(magic);
		result.setCmd(cmd);
		result.setData(name);
		return result;
	}

	public static NotifyMessage formNotifyMessage(String magic, String name, String cmd, int state) {
		NotifyMessage result = new NotifyMessage();
		result.setMagic(magic);
		result.setCmd(cmd);
		result.setData(Integer.toString(state) + Prefix.SEPARATOR_STRING + name);
		return result;
	}

	public static NameChangeMessage formNameChangeMessage(String magic, String name, String newName) {
		NameChangeMessage result = new NameChangeMessage();
		result.setMagic(magic);
		result.setCmd(Prefix.NOTIFY_NAME_CHANGE);
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append(Prefix.NOTIFY_NAME_CHANGE_STATE)
				.append(Prefix.SEPARATOR_STRING)
				.append(name)
				.append(Prefix.SEPARATOR_STRING)
				.append(newName);
		String message = messageBuilder.toString();
		result.setData(message);
		return result;
	}

}