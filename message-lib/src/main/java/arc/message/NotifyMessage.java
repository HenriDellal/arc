package arc.message;

public class NotifyMessage extends Message {
	protected int state;

	public static int DATA_COUNT = 2;


	public int getState() {
		return state;
	}

	@Override
	public void setData(String data) {
		String[] dataParts = data.split(Prefix.SEPARATOR_STRING, DATA_COUNT);
		state = Integer.parseInt(dataParts[0]);
		name = dataParts[1];
	}

	@Override
	public String getData() {
		StringBuilder sb = new StringBuilder();
		sb.append(state).append(Prefix.SEPARATOR_STRING).append(name);
		return sb.toString();
	}
}
