package arc.message;

public class ClientEntity {
	private String formattedTime;
	private String name;
	private StatsMessage stats;
	private int state;

	public ClientEntity(String name) {
		this.name = name;
	}

	public void updateTime(String time) {
		formattedTime = time;
	}

	public void setState(Message message) {
		if (!(message instanceof NotifyMessage)) {
			return;
		}
		NotifyMessage nm = (NotifyMessage) message;
		state = nm.getState();
	}

	public int getState() {
		return state;
	}

	public String getName() {
		return name;
	}

	public String getFormattedTime() {
		return formattedTime;
	}

	public void setStats(StatsMessage stats) {
		this.stats = stats;
	}

	public String getStatsData() {
		return (null != stats) ? stats.getData() : null;
	}

	public void setName(String newName) {
		name = newName;
	}
}
