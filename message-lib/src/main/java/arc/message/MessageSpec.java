package arc.message;

public class MessageSpec {
	public static final int MAGIC_SIZE = 9;
	public static final int PROTOCOL_VERSION_SIZE = 1;
	public static final int CMD_SIZE = 16;
	public static final int CMD_DATA_SIZE_SIZE = 2;
	public static final int MAX_CMD_DATA_SIZE = 1000;
}